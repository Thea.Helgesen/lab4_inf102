package INF102.lab4.median;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int n = listCopy.size(); // size of list

        int medianIndex = n / 2; // middle index of list
        int low = 0;
        int high = n - 1;

        while (low <= high) { // QuickSelect algorithm
            int pivotIndex = partition(listCopy, low, high); // pivot: middle element 

            if (pivotIndex == medianIndex) { // if pivot index is the median index
                return listCopy.get(pivotIndex); // return median
            } else if (pivotIndex < medianIndex) { // if pivot index is less than median index
                low = pivotIndex + 1; // search right side of list
            } else {
                high = pivotIndex - 1; // search left side of list
            }
        }
        return null; // if no median is found
    }

    private <T extends Comparable<T>> int partition(List<T> list, int low, int high) { 
        T pivot = list.get(high); // pivot: last element
        int i = low; // index of smaller element

        for (int j = low; j < high; j++) { // for each element in list
            if (list.get(j).compareTo(pivot) <= 0) { // if element is <= pivot
                Collections.swap(list, i, j); // swap element with element at index i
                i++; // 
            }
        }
        Collections.swap(list, i, high); // swap element at index i with pivot
        return i; // return index of pivot
    }
}

// Worst case: the QuickSelect algorithm can have a time complexity of O(n^2), if the pivot chosen in each partition is always 
// the smallest or largest element in the current subarray, leading to unbalanced partitions.

